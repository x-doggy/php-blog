<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// PHP main functions

error_reporting(E_ALL);
ini_set('display_errors', 1);


class db {
    private $last;

    public function __construct() {
        $current_dirname = dirname(__FILE__);
        $dsn = "sqlite:$current_dirname/blog.db";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->conn = new PDO($dsn, null, null, $opt);
    }

    public function __destruct() {
        $this->close();
    }

    public function query($sql) {
        // $db->query("SELECT * FROM blog_main WHERE id = ?", $id);

        $args = func_get_args();       // массив аргументов ф-ции
        $sql = array_shift($args);     // извлекаем первый элемент массива
        $link = $this->conn;

        $args = array_map(function ($param) use ($link) {
            return "'".$link->escape_string($param)."'";
        }, $args);

        $sql = str_replace(array('%','?'), array('%%','%s'), $sql);
        array_unshift($args, $sql);
        $sql = call_user_func_array('sprintf', $args);

        $this->last = $this->conn->query($sql);
        if ($this->last === false) throw new Exception('Database error: '.$this->conn->error);

        return $this;
    }

    public function assoc() {
        return $this->last->fetch();
    }

    public function all() {
        $result = array();
        while ($row = $this->last->fetch())
            $result[] = $row;
        return $result;
    }

    public function close() {
    }
}


/**
 * @param $title string document title
 * Inserts head of blog
 */
function head($title, $type) {
    include_once('parts/head.php');
}

/**
 * Output footer of all pages
 */
function foot() {
    include_once('parts/foot.php');
}

/**
 * @param $datetime string format YYYY-mm-dd H:i:s
 * @return string altered datetime
 * Inserts 'T' after day
 */
function to_html_time($datetime) {
    $datetime[10] = 'T';
    return $datetime;
}

/**
 * @param $datetime string format YYYY-mm-dd H:i:s
 * @return string altered datetime
 * Inserts ' ' after day
 */
function to_normal_time($datetime) {
    $datetime[10] = ' ';
    return $datetime;
}

/**
 * @param string string page number parse from
 * @return int parsed page number
 * Parse page number from URL
 */
function get_page_number($str = null) {
    $req = &$_SERVER['REQUEST_URI'];
    $str = $str or $req;
    $page = 1;
    $url_parsed = parse_url($req); // парсим url-адрес
    if (!is_null($url_parsed) && array_key_exists('query', $url_parsed)) { // на первой странице нет ключа 'query'
        $strquery = $url_parsed['query'];
        if (strpos($strquery, 'page=') === FALSE) {
            $page = (int)$strquery;
        } else {
            $page = (int)substr($strquery, 5); // берем всё, что после подстроки "page="
        }
    }
    return $page;
}