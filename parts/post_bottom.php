<table class="x-share x-mod__mrgn-top">
<tr>
    <td>
        <p class="x-mod__dim"><i><b>Сслыка на эту страницу: </b> <span class="x-mod__underline"><?php echo $actual_url; ?></span></i></p>
    </td>
    <td style="text-align: right; width: 260px;">
        <a href="rss.php">RSS</a>
        <!-- Блок "Поделиться" Яндекса -->
        <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
        <script src="//yastatic.net/share2/share.js" charset="utf-8"></script>
        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,blogger,lj" data-size="s"></div>
    </td>
</tr>
</table>

<nav class="x-nav-prev-next">
    <table class="x-nav__tbl">
        <?php if ($page > 1): ?>
        <col width="11">
        <col width="50%">
        <?php endif; ?>
        <?php if ($page+1 <= $total): ?>
        <col width="50%">
        <col width="11">
        <?php endif; ?>
        <tr>
            <?php if ($page > 1): ?>
            <td width="20"><i>‹</i></td>
            <td>
                <i><a href="post.php?<?php echo ($page-1); ?>" title="Предыдущий пост"><?php echo $title_prev; ?></a></i>
            </td>
            <?php endif; ?>
            <?php if ($page+1 <= $total): ?>
            <td>
                <i><a href="post.php?<?php echo ($page+1); ?>" title="Следующий пост"><?php echo $title_next; ?></a></i>
            </td>
            <td width="20"><i>›</i></td>
            <?php endif; ?>
        </tr>
    </table>
</nav>
