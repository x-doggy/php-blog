<!DOCTYPE html>
<html>
<head>
<title><?php echo $title; ?></title>
<meta charset="utf-8">
<link rel="stylesheet" href="css/style.css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php "rss.php"; ?>">
</head>

<body class="x-body__<?php echo $type; ?>">
<div class="x-wrap">
    <hgroup class="x-hgr__title">
        <h1 class="x-title__main"><a class="x-title__link" href="index.php">Блог Владимира Стадника</a></h1>
        <h2 class="x-title__secondary">На самописном двигле</h2>
    </hgroup>
