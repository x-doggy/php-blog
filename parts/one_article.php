
<article class="x-one-article">
    <hgroup class="x-hgr__message">
        <h3 class="x-title__post"><a href="post.php?<?php echo $index; ?>"><?php echo $arr[$i]['title']; ?></a></h3>
        <h4 class="x-title__when">Опубликовано: <time datetime="<?php echo to_html_time($arr[$i]['datetime']); ?>" class="x-pubtime"><?php echo $arr[$i]['datetime']; ?></time></h4>
    </hgroup>
    <p><?php echo $content; ?></p>
</article>
