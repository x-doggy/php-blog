<?php if ($pages > 1): ?>

<nav class="x-nav-pages">
    <?php for ($i=1; $i<=$pages; $i++): ?>
        <?php if ($i == $page): ?>
        <i class="x-nav-pages__links"><b><?php echo $i; ?></b></i>
        <?php else: ?>
        <a href="?page=<?php echo $i; ?>" class="x-nav-pages__links"><?php echo $i; ?></a>
        <?php endif; ?>
    <?php endfor; ?>
</nav>

<?php endif; ?>