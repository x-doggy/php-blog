
<hgroup class="x-hgr__message">
    <h3 class="x-title__post"><?php echo $arr['title'];?></h3>
    <h4 class="x-title__when">
        Опубликовано:
        <time datetime="<?php echo $time; ?>" class="x-pubtime"><?php echo $arr['datetime']; ?></time>
    </h4>
</hgroup>

