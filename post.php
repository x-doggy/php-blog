<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс страницы с одним сообщением

require_once('fn.php');

$page = get_page_number();

@$db = new db;
$arr = $db->query("SELECT title, datetime FROM blog_main WHERE id = $page")->assoc();

head("$arr[title] | Блог Владимира Стадника", "post");
$time = to_html_time($arr['datetime']);

include_once("parts/hgr_message.php"); // Выводим заголовок и дату создания сообщения
include_once("bodies/body$page.html");

$total      = $db->query("SELECT COUNT(*) FROM blog_main")->assoc()['COUNT(*)'];
$title_prev = $db->query("SELECT title FROM blog_main WHERE id = ($page-1)")->assoc()['title'];
$title_next = $db->query("SELECT title FROM blog_main WHERE id = ($page+1)")->assoc()['title'];
$actual_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// Выводим предыдущую и следующую страницы, а также блок "поделиться"
include_once('parts/post_bottom.php');

//$db->close();
foot();
