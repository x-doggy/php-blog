<?php
// �2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// ��������� RSS-�����

// ���������� �������� XML
header("Content-type: text/xml; charset=windows-1251");

/** @var string $lastBuildDate
 * ���� ��������� ������ ����.
 */
$lastBuildDate = date("D, j M Y G:i:s") . " GMT";

echo <<<END
<?xml version="1.0" encoding="windows-1251"?>
<rss version="2.0">
<channel>
    <title>blog.stadnik.name RSS feed</title>
    <link>http://blog.stadnik.name</link>
    <description>���� ��������� ��������</description>
    <pubDate>$lastBuildDate</pubDate>
    <lastBuildDate>$lastBuildDate</lastBuildDate>
    <docs>http://blogs.law.harvard.edu/tech/rss</docs>
    <generator>PHP Storm and my brain</generator>
    <copyright>Copyright 2016 Vladimir Stadnik</copyright>
    <webMaster>x-doggy@ya.ru</webMaster>
    <language>ru</language>
END;

require_once('fn.php');

$res = (new db)->query("SELECT * FROM blog_main ORDER by datetime DESC LIMIT 0,10");

while ( $row = $res->assoc() ) {
    $index   = $row['id'];
    $title   = mb_convert_encoding( strip_tags( trim($row['title'] ) ), "CP1251", "UTF-8" );
    $anon    = mb_convert_encoding( trim( strip_tags( file_get_contents("bodies/body$index.html", NULL, NULL, 0, 1024) ) ), "CP1251", "UTF-8" );
    $pubDate = $row['datetime'];
    echo <<<END
    
    <item>
        <title>$title</title>
        <description><![CDATA[$anon]]></description>
        <link>localhost:80/php_blog/post.php?$index</link>
        <guid isPermaLink="true">localhost/php_blog/post.php?$index</guid>
        <pubDate>$pubDate</pubDate>
    </item>
END;
}

echo "</channel>
</rss>";
