<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс главной страницы

require_once('fn.php');
head("Блог Владимира Стадника", "index");

// Число сообщений на каждой странице
define('MSGS_ON_PAGE', 9);

$page = get_page_number();

@$db = new db;
$total = $db->query("SELECT COUNT(*) FROM blog_main")->assoc()['COUNT(*)']; // получаем общее кол-во записей блога
$pages = ceil( $total / MSGS_ON_PAGE ); // на сколько страниц поделить блог
$arr = $db->query("SELECT * FROM blog_main WHERE id <= ".($total - MSGS_ON_PAGE*($page-1))." ORDER BY id DESC LIMIT ".MSGS_ON_PAGE)->all();

for ($i=0, $cnt=count($arr); $i<$cnt; $i++) {
    $index = $total - $i - MSGS_ON_PAGE * ($page - 1);
    $content = htmlspecialchars( strip_tags( file_get_contents("bodies/body$index.html", NULL, NULL, 0, 1024) ), ENT_NOQUOTES | ENT_HTML5 | ENT_IGNORE ).'...';
    include('parts/one_article.php'); // Выводим заголовок, дату создания сообщения и часть сообщения
}

// Выводим список страниц
include_once('parts/pages_list.php');

//$db->close();
foot();
