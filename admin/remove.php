<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс редактирования записи

require_once('../fn.php');
head('Удаление записи', 'admin');

$msgid = $_GET['msg'];
$total = $_GET['total'] - 1;

@$db = new db;
$db->query("DELETE FROM blog_main WHERE id = $msgid");       // Удаляем запись в базе данных
$db->query("ALTER TABLE blog_main AUTO_INCREMENT = $total"); // Следующее добавленное сообщение должно начинаться с (this id) - 1

unlink("../bodies/body$msgid.html");                         // Удаляем физический файл
echo "<p>Запись удалена успешно!</p>";

foot();
