<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс административной панели

require_once('../fn.php');
head('Админка блога Владимира Стадника', 'admin');

$arr = (new db)->query("SELECT * FROM blog_main")->all();

// Выводим таблицу сообщений
include_once('../parts/admin_index.php');

foot();