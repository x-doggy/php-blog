<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс добавления сообщения

require_once('../fn.php');
head('Интерфейс добавления сообщения | Админка блога Владимира Стадника', 'admin');
?>

<style>

</style>

<h3><a href="admin/index.php">Админка</a></h3>
<form enctype="multipart/form-data" class="x-wrap x-frm x-tabs" action="admin/ins.php" method="post" id="fmText">
    <input name="msg_title" class="x-msg__title" placeholder="Название сообщения блога" autofocus required>

    <input type="radio" name="uploadtype" value="bytext" id="rdText" checked>
    <label for="rdText">HTML-текстом</label>
    <input type="radio" name="uploadtype" value="byfile" id="rdExistsfile">
    <label for="rdExistsfile">Через файл на диске</label>

    <main class="x-tabcontent">
        <section class="x-tabset" id="fmText">
            <h4>Добавление сообщения посредством HTML-текста</h4>
            <textarea name="msg_body" class="x-msg__body" placeholder="HTML блога здесь"></textarea>
        </section>

        <section class="x-tabset" id="fmExists">
            <h4>Добавление сообщения посредством файла</h4>
            <input type="file" name="fileMessage" value="exists" placeholder="Укажите файл на диске">
        </section>
    </main>
    <div><input name="msg_enter" class="x-msg__enter" value="Отправить!" type="submit"></div>
    <div style="margin-top: 2em;"><a href="/php_blog">Просмотреть блог</a></div>
</form>

<?php
foot();
