<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс страницы ответа после вставки сообщения в БД

require_once('../fn.php');
head("Ответ на запрос", "ins");

if (!empty($_POST)) {
    $title = $_POST['msg_title'];
    $now = date("Y-m-d H:i:s");
    @$db = new db;
    $db->query("INSERT INTO blog_main (title, datetime) VALUES (?, ?)", $title, date("Y-m-d H:i:s"));

    switch ($_POST['uploadtype']) {
        case 'bytext':
            $body = addslashes($_POST['msg_body']);
            $fileid = $db->query("SELECT id FROM blog_main WHERE title = $title")->assoc()['id'];
            $handle = fopen("../bodies/body$fileid.html", 'w') or die("Не могу открыть файл для записи!");
            fputs($handle, stripslashes($body)) or die("Не могу записать в файл!");
            fclose($handle);
            echo <<<END
    <p>Сообщение успешно занесено!</p>
    <p><a href=\"/php_blog/\">Посмотреть сообщения блога</a></p>
END;
            break;

        case 'byfile':
            define('UPLOAD_DIR', '../bodies/');
            $total = $db->query("SELECT COUNT(*) FROM blog_main")->assoc()['COUNT(*)'];
            $uploadfile = UPLOAD_DIR . "body$total.html";
            echo "<pre>";
            if (is_uploaded_file($_FILES['fileMessage']['tmp_name'])
                &&
                move_uploaded_file($_FILES['fileMessage']['tmp_name'], $uploadfile)
            )
                echo "Файл ", $_FILES['fileMessage']['name'], " успешно загружен.\n";
            echo "<p>Некоторая отладочная информация:</p>";
            print_r($_FILES);
            echo "</pre>";
            break;

        default:
            die('Something\'s wrong with upload type!');
    }

    foot();
}
