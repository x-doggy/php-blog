<?php
// ©2016 Vladimir Stadnik, <mailto:x-doggy@ya.ru>
// Интерфейс редактирования записи

require_once('../fn.php');
head('Редактирование записи', 'admin');

@$db = new db;

if (!empty($_POST['msg_ok'])) {
    $db->query("UPDATE blog_main SET title=$_POST[msg_title], datetime='".to_normal_time($_POST['msg_date'])."' WHERE id=$_POST[msg]");
    // Rewrite a file
    $handle = fopen("../bodies/body$_POST[msg].html", 'w') or die("Не могу открыть файл для записи!");
    fputs($handle, html_entity_decode($_POST['msg_body'])) or die("Не могу записать в файл!");
    fclose($handle);
    echo "<p>Контент обновлён успешно!</p>";
    return;
}

$arr = $db->query("SELECT * FROM blog_main WHERE id=$_GET[msg]")->assoc();
?>

<h3><a href="admin/index.php">Админка</a></h3>
<form action="admin/edit.php" method="post">
    <div><input name="msg_title" value="<?php echo $arr['title']; ?>" size="80"></div>
    <div><input name="msg_date" value="<?php echo to_html_time($arr['datetime']); ?>" type="datetime-local"></div>
    <div><textarea name="msg_body" cols="80" rows="15"><?php echo htmlentities(file_get_contents("../bodies/body$arr[id].html"), ENT_QUOTES); ?></textarea></div>
    <div><input type="submit" name="msg_ok" value="Готово!"></div>
    <input type="hidden" name="msg" value="<?php echo $_GET['msg']; ?>">
</form>

<?php
foot();
